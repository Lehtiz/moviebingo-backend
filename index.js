const express = require('express')
const app = express()
const data1 = require('./public/bingodata.json');
const data = require('./public/bingo.json');

const keyIndex = require('react-key-index');

var fs = require('fs');
var file = './public/bingo.json'


app.use(express.static('public'));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var bodyParser = require('body-parser')
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));


// GET method to return data for the app
app.get('/', function (req, res) {
  res.send('Should not be here. <a href="http://localhost:3000/">Back</a>')
})

app.get('/arraysize', function (req, res) {
  var obj
  fs.readFile(file, 'utf8', function (err, data) {
    if (err) throw err;
    obj = JSON.parse(data);
    res.send(obj.bingo.length)
  });
})

app.get('/getAllData', function (req, res) {
  var obj
  fs.readFile(file, 'utf8', function (err, data) {
    if (err) throw err;
    obj = JSON.parse(data);
    res.send(obj.bingo)
  });
})

app.get('/getAllTopics', function (req, res) {
  var obj
  var myTopicSet = new Set();
  fs.readFile(file, 'utf8', function (err, data) {
    if (err) throw err;
    obj = JSON.parse(data);
    //loop through parsed json
    obj.bingo.forEach((data) => {
      //add entries to a set (can not have duplicates)
      myTopicSet.add(data.category)
    })
    //return an array of topics
    res.send(Array.from(myTopicSet))
  });
})

app.get('/getFilteredData', function (req, res) {0
  var obj
  var filterCategory = req.query.category

  fs.readFile(file, 'utf8', function (err, data) {
    if (err) throw err;
    obj = JSON.parse(data);

    var newarr = []
      for(var i = 0; i < obj.bingo.length; i++ ){
        if(obj.bingo[i].category === filterCategory){
          newarr.push(obj.bingo[i].content)
        }
      }
    res.send(newarr)
  });
})

// POST method to save new data
app.post('/saveInput', function (req, res) {
  var category = req.body.category;
  var content = req.body.content;

  var input = {
    category: category,
    content: content
  };

  //make sure cat and cont have something in them
  if (req.body.category.length > 0 && req.body.content.length > 0){
    saveDataToFile(input , file, function(err){
      if (err) {
       res.status(404).send('Input not saved. <a href="http://localhost:3000/">Back</a>');
       return;
      }
      res.send('Input saved. <a href="http://localhost:3000/">Back</a>')
    })
  }
})

function saveDataToFile(input, file, callback){
  var obj

  fs.readFile(file, 'utf8', function (err, data) {
    if (err) throw err;
    //read old data
    obj = JSON.parse(data);
    //add new data
    obj.bingo.push(input)
    //save updated file
    fs.writeFile(file, JSON.stringify(obj), callback);
  });
}



app.listen(3001, () => console.log('Example app listening on port 3001!'))
